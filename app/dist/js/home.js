button = document.querySelector('.main-content-btn');
meme = document.querySelector('.main-content-img');
showing = false;

button.onclick = function(e) {
    if (showing) {
        meme.src = '';
        meme.style.display = 'none';
        showing = false;
    } else {
        assetNum = Math.floor((Math.random() * 5) + 1);
        selection = '';

        switch (assetNum) {
            case 1: selection = 'assets/img/kermit.gif'; break;
            case 2: selection = 'assets/img/dragon.gif'; break;
            case 3: selection = 'assets/img/kiss.gif'; break;
            case 4: selection = 'assets/img/lines.gif'; break;
            case 5: selection = 'assets/img/physics.gif'; break;
            case 6: selection = 'assets/img/whlub.gif'; break;
        }
        meme.src = selection;
        meme.style.display = 'initial';
        showing = true;
    }
};
