// Node Modules
const bs = require('browser-sync');
const express = require('express');
const jade = require('jade');
const morgan = require('morgan');
const stylus = require('stylus');

// User variables
const app = express();
const port = 2368;
const viewPath = '/app/src/jade';
const stylePath = '/app/src/stylus';
const styleOutPath = '/app/dist/css';

function compile(str, path) {
    return stylus(str)
            .set('filename', path)
            .set('compress', true)
};

// Our Express server configuration
app.set('views', __dirname + viewPath);
app.set('view engine', 'jade');
app.use(morgan('combined'));
app.use(stylus.middleware({
        src: __dirname + stylePath,
        dest: __dirname + styleOutPath,
        compress: true,
        compile: compile
    })
);

// Static server for all files
app.use(express.static(__dirname + '/app/dist'));

// Our sites routes
app.get('/', function(req, res) {
    res.render('home');
});

app.get('/wall', function(req, res) {
    res.render('wall');
});

app.get('*', function(req, res) {;
    res.render('404');
});

// Let's listen and setup the server
app.listen(port, function() {
    bs({
        files: ['app/src/*/*.{jade,styl,json}'],
        open: false,
        proxy: 'localhost:' + port
    });
    console.log('Listening on port ' + port + ' w/ browser-sync');
});
